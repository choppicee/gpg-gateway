* Names of files being transferred can't contain whitespace
* Don't send files with duplcate filenames.  Can't guarantee they won't be overwritten if duplcate filenames sent in quick succession
* Need to set up gpgadmin mail alias
* If encrypt = "false" then transfers won't work if files have a .gpg extension
